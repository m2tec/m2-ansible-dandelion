# m2-ansible-dandelion

## Getting started

Install ansible 
sudo apt-get install ansible

git clone https://gitlab.com/m2tec/m2-ansible-dandelion.git
cd m2-ansible-dandelion

Get a server running somewhere. This ansible script is based on ubuntu

Modify the m2-ansible-dandelion/prod/inventory.yaml file to match your server configuration. 
This is an example where the server is running on localhost and you login under user ubuntu. Change the ssh key to the one you are using to access your server:

      ansible_host: 192.168.1.101
      ansible_user: ubuntu
      ansible_become_pass: 'ubuntu'
      ansible_ssh_private_key_file: ~/.ssh/keyfile

Run the ansible script with:
./install      